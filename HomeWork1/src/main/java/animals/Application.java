// for git
package animals;

public class Application {

    public static boolean compareVoice (animals.Kotik k1, animals.Kotik k2) {
        return k1.getVoice() == k2.getVoice();
    };

    public static void main(String[] args) {
        animals.Kotik kotik1 = new animals.Kotik("Barsik", "meow", 5, 7);
        animals.Kotik kotik2 = new animals.Kotik();

        kotik2.setName("Murzik");
        kotik2.setVoice("purr");
        kotik2.setSatiety(3);
        kotik2.setWeight(6);

        for (String day: kotik1.liveAnotherDay()) {
            System.out.println(day);
        }

        System.out.println("Кличка котика " + kotik2.getName() + ", вес " + kotik2.getWeight());
        System.out.println(animals.Kotik.count);

        if (compareVoice(kotik1, kotik2))
            System.out.println("Котики разговаривают одинаково");
        else
            System.out.println(" Котики разговаривают по-разному");

    }

}
